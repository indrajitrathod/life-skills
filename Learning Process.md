# Learning Process

## 1. How to Learn Faster with the Feynman Technique

### What is the Feynman Technique?

Feynman was known as the “**Great Explainer.**”<br/>
His technique for teaching and communication is a mental model to convey information using concise thoughts and simple language.<br/> 
It is a method for learning or reviewing a concept quickly by explaining it in plain, simple language. It is a **four-step process**.

1. **Identify the subject** - Write down everything you know about the topic. Each time you run into new sources of information, add them to the note.
2. **Teach it to a child** - write it plainly and simply, so that even a child can understand what you’re talking about.
3. **Identify your knowledge gaps**-  This is the point where the real learning happens. What are you missing? What don’t you know?
4. **Organize + simplify + Tell a story**- Use analogies and simple sentences to strengthen your understanding of the story.

## Question 2

### What are the different ways to implement this technique in your learning process?

The different ways to implement Feynman Techniques in my learning process are:


1. Choose a subject, make proper notes, and keep add-ons.
2. Giving content to someone and asking them if they understands it.
3. Add stories and analogies to simplify the concept.
4. Knowing properly, what I know, and what I don't.

## Question 3

### Paraphrase the video in detail in your own words.

The video explains about the learning process.<br/> 
Brain works in two modes 
 - **Focus mode**- where, we pay attention and listen.
 - **diffuse mode** - where, we be in relaxed state.
when we are learning anything, we go fro and to between these two modes. when we are learning something new or solving something for a long time, we need to put our mind in a relaxed state and learn.

Procrastination is the act of delaying or putting off tasks until the last minute, or past their deadline.
 
 We feel physical pain in the part of our brain that analyzes pain. This pain is handled in **two ways**:
  - By procrastinating
  - By working through it.
 
 To overcome procastination, **Pomodoro Technique** can be used. It is a way to handle procrastination, by focusing on a work for around 25 minutes, by setting up a timer. and then entering into relaxed state for few minutes. 

## Question 4

### What are some of the steps that you can take to improve your learning process?

- Prepare my own notes.
- Adding new contents, as and when I find them useful.
- Planning, organising, and maintaining a proper time-table.
- Meeting deadlines and tatking tests.
- Creating analogies and mnemonics to conceptualize complete comcept.
- Use Pomodoro Technique when I'm feeling lazy to do some work.

## Question 5

### Your key take-aways from the video? Paraphrase your understanding.

- Selecting a skill we want to develop upon 
- Using divide-and-conquer approach. Dividing it into smaller pieces, approaching them one by one, conquer those concepts first that we can handle.
- Get a few resources on the skill, and practice, ad it would help us by self-correcting.
- Removing all distractions that hinder.
- Approaching and handling calmly, than getting frustated.

## Question 6

### What are some of the steps that you can while approaching a new topic?

- Using divide and conquer techique, to chunk down to smaller topics and understand them.
- Focusing with full attention, as first-time well-learnt is better than repeated half-understaning.
- Referring the standard documents, or websites.
- Keeping consistency.
  
---
 ## References
- [Learn Faster with the Feynman Technique](https://www.youtube.com/watch?v=_f-qkGJBPts)  
- [How to Learn TED talk by Barbara Oakley](https://www.youtube.com/watch?v=O96fE1E-rf8)
- [Learn Anything in 20 hours](https://www.youtube.com/watch?v=5MgBikgcWnY)