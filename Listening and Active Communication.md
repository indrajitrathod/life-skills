# Listening and Active Communication

## 1. Active Listening
The act of fully hearing and comprehending the meaning of what someone else is saying.

#### Q1. What are the steps/strategies to do Active Listening?    

- Avoid getting distracted by your own thoughts. 
- Focus on the speaker and topic.
- Try not to interrupt the other person. 
- Let them finish and then respond.
- Use door openers. These phrases show you are interested and keep the other person talking. Like,
  - Tell me more.
  - Go ahead. I'm listening.
  - That sounds interesting.

 - Show that you are listening with body language.
 - If appropriate take notes during important conversations.
 - Paraphrase what other have said to make sure you are on the same page.

## 2. Reflective Listening
 #### Q2. According to Fisher's model, what are the key points of Reflective Listening?
 - Focusing upon the conversation by reducing or eliminating distraction.
- Embracing the speaker's perspective genuinely. 
- Encourage speaker to speak freely.
- Mirroring the mood of the speaker, with both words and non-verbal communication, with body movements, eye-contacts and gestures.
- Summarize what the speaker said 
- Use speaker's own words, instead of re-ordering words and phrases.
- Repeat the procedure for each subject
- Switch the roles between speaker and listener.
- In this approach, both client and therapist embrace the technique of thoughtful silence, rather than to engage in idle chatter.

## 3. Reflection
 #### Q3. What are the obstacles in your listening process? 
   - Can’t understand fast.
   - Trouble with different accent.
   - Tendency to understand every word. 
  
#### Q4. What can you do to improve your listening? 
   - Paying more attention.
   - Not getting distracted in own thoughts.
   - Asking question to the speaker, if any trouble.
   - Try to be genuine and not fake as understood.

## 4. Reflective Listening
 #### Q5. When do you switch to Passive communication style in your day to day life?

 - When I feel uneasy or not well.
 - When speaking to someone very authoritative, or dominant
 - when conversation is too boring.
 - If I have some other important work to meet deadline, to quickly end the conversation.
 
 #### Q6. When do you switch into Aggressive communication styles in your day to day life?

 - When conversation is offending.
 - If I am being irritated or troubled, and not stopping even after saying not to.
 - when someone forces me to do something, despite declining repeatedly.
 - When somone tries to be aggressive, and not listen in peaceful way repeatedly.
 - If someone tries to harass. 

  #### Q7. When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

 - For fun, with very close friends.
 - when one don't listen, depsite saying politely often.
 - When someone try to invade my privacy.
 - If someone try to harass everyone around.

#### Q8. How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life?

 - By trying to recognize and name the feelings that arise.
 - Recognize needs that I want to address to the person that I am conversing with.
 - By practicing them with people that I feel comfortable with.
 - By being aware of the body language.
 - Avoiding to be aggressive.

---
 ## References
- [Active Listening](https://www.youtube.com/watch?v=rzsVh8YwZEQ)  
- [Reflective listening](https://en.wikipedia.org/wiki/Reflective_listening)
- [Types of Communication](https://www.youtube.com/watch?v=yjOWXsDt87Y)
- [Spongebob Assertiveness Training](https://www.youtube.com/watch?v=SYuboi4GWO4)
- [How passive communication leads to aggressive communication](https://www.youtube.com/watch?v=BanqlGZSWiI)
- [Tips on Assertive Communication](https://www.youtube.com/watch?v=vlwmfiCb-vc)
