# Energy Management

## 1. Manage Energy not Time

### Q1. What are the activities you do that make you relax - Calm quadrant?

Some of the activities that make me relaxed are
- Watching movies
- Photography, or Drawing
- Swimming
- Trekking in forest or on hill top
- Drinking a glass of water or some juice, when stressed

### Q2. When do you find getting into the Stress quadrant?

- When I don't meet my own expectation.
- When I approach deadline of a task, if I have not completed or near-completed the task.
- When my family or friends are hospitalised, or met with any severe accident.

### Q3. How do you understand if you are in the Excitement quadrant?
- When I feel satisfied by meeting my expection.
- When I feel happy.
- When I realise, I solved few problems.
- When I am excited to learn something new.
- When I am excited meet someone close and dear-ones after a long time.

## 4. Sleep is your superpower
   
### Q4. Paraphrase the Sleep is your Superpower video in detail.
The video explains the importance of sleep, and problems related to lack of sleep.

Dissemination of sleep has catastrophic effect on health, and wellness. It is going to be one of the greatest public-health challenges in the 21st century.
- Lack of sleep, will age a man by a decade in the critical aspect of wellness.
- Females will have deteriated reproductive health, due to lack of sleep.
- You need sleep after studing, to save it in brain, as welll as before studing to prepare the brain to study(to absorb new information). Lack-of-sleep can lead to learning disability.
- It was found in a research that, sleep shifts memory vulnerable reservior to a more long-term storage space in the brain.
- There is a physiological signature of aging with lack of sleep.
- Disruption of Deep sleep is an under-appreciated factor lead to cognitive/memory decline.
- Sleep loss for Cardio-vascular system
  - As per a research, it was found that, due to day-light-saving mechanism in cold-temperate countires, In spring, when they lose one-hour of sleep, there is an increase of 21% Heart attacks.<br/>And In Autumn, when they get 1hour of extra sleep, Heart-attacks gets reduced by 21%.

- Similar kind of data was found for car-crashes, and even sucides rates as well.
- Lack of sleep destroys natural immune cells. 
- WHO has classified any form of night-time shift work as probable-carcinogenic, because of disruption of sleep-rhythm.
- Shorter the sleep, shorter the life.
- Short-sleep predicts, or cause mortality.
- Lack of sleep will even erode, the very fabric of Biological-life itself.

**Sleep unfortunately, is not an optional-life-style or luxury, sleep is a non-negotiable, biological neccessity**
  
### Q5. What are some ideas that you can implement to sleep better?
- Avoiding electronic devices like mobile and laptop, one for before sleep.
- Regularity in sleep: Going to bed at same time, and  waking up at the same time. Regularity improves, quantity as well as quality of sleep.
- Sleep in relatively lower temperature room. Because, body falls to sleep better in relative-cooler environment.
- Meditating for 10 minutes before the sleep.

## 5. Brain Changing Benefits of Exercise

### Q6. Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.
The video explains the powerful effects of Physical Activities.
- Simply, moving your body has immediate, long-lasting and protective benefits for the brain, which lasts for rest of the life.
- Boosts our mood and energy.
- Helps to maintain focus and attention for long period.
- Physical activities increases neurotransmitters like dopamine, serotonin, and noradrenaline.
- Improves reaction time.
- Protects brain from neurodegenerative diseases.

**Exercise is the most transformative that one can do for the brain.**

### Q7. What are some steps you can take to exercise more?
 - Exercising daily in the morning or evening.
 - Playing some sports with friends, whenever possible.
 - Playing on weekends
 - Taking a small walk in-between stressful work in intervals, which relaxes.

---
 ## References
- [Headspace | Meditation | Brilliant things happen in calm minds](https://www.youtube.com/watch?v=lACf4O_eSt0)
- [One-Moment Meditation: "How to Meditate in a Moment"](https://www.youtube.com/watch?v=F6eFFCi12v8)
- [Benefits of Meditation](https://www.healthline.com/health/mental-health/types-of-meditation)
- [Sleep is your superpower | Matt Walker](https://www.youtube.com/watch?v=5MuIMqhT8DM)
- [Wendy Suzuki: The brain-changing benefits of exercise](https://www.youtube.com/watch?v=BHY0FxzoKZE)
  