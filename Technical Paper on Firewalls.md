## What is a Firewall?

A firewall is a network security system, which monitors incoming and outgoing network traffic, and controls them by allowing or blocking based on predetermined security rules.   

A firewall's primary task is to regulate traffic flow between different trust levels.

![A firewall illustration](/assets/images/firewall-diagram.webp)

A firewall can be hardware, software, or both.

### Software Firewall vs Hardware Firewall

| No. | Software Firewall                                                                                                                          | Hardware Firewall                                                                                                                                                                                                           |
| --- | ------------------------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1.  | It is a program on a single computer. Hence, it's also called a Host Firewall.                                                             | It is a device between your local network (LAN) and Untrusted internet (WAN). It is also called an Appliance Firewall.                                                                                                      |
| 2.  | It protects a single computer.                                                                                                             | It protects the entire Local Area Network (LAN).                                                                                                                                                                            |
| 3.  | This is necessary to be installed, as it helps to protect the computer if another computer within the same network is affected by viruses. | It is necessary to install, as it helps to protect the computer if another computer within the same network is affected by viruses. It protects the complete local network, like your home network, from insecure internet. |


### Advantages & Disadvantages of Software Firewall
- #### Advantages
    1. It's cheaper.
    2. It is ideal for personal or home use.
    3. Easy to configure or reconfigure. 
    <br />

- #### Disadvantages
    1. It takes up system resources.
    2. Difficult to install and upgrade.
    3. Not suitable where response times are critical.

### Advantages & Disadvantages of Hardware Firewall
- #### Advantages
    1. **Better speed and can handle more traffic:** It is tailored for faster response times and hence, can drive more traffic loads.
    2. **More Secure:** A firewall with its operating system (proprietary) is less prone to attacks. Which, in turn, reduces the security risk.
    3. **No Interference:** Since dedicated hardware, which is separate, doesn't slow down other applications.
    <br />

- #### Disadvantages
    1. It's costlier.
    2. Difficult to install and upgrade.
    3. It takes up physical space and involves wiring.


## How does a firewall work?
A firewall protects us from becoming a victim of an online hacker. 

![Firewall working](/assets/images/hacker2.webp)

Let's try to understand how the firewall does that.


### Prerequisite Knowledge -
- **Data Packet**: A data packet is a fundamental communication unit over a digital network.  
When computers communicate over the network, large data files are chunked into these smaller units called data packets and transformed over the network.

![Data Packes in network](/assets/images/packets.png)  

A data packet has a **source address, destination address,** and other information.

![Ipv4 Data Packet Structure](/assets/images/tcp_ip_header_ipv4.jpg)
  
So, when a data packet transmits over a network, the firewall can inspect the source & destination address and can regulate the data based on predetermined security rules.


### Types of Firewall
1. **Packet filtering firewall:** 
     - A packet filtering firewall maintains a list of rules called **Access Control List**, which filters the data packets, allowing or rejecting the packets to pass through.
  
      ![Access Control List](/assets/images/ACL.jpg) 

   - Packet filtering firewall checks for sender's and receiver's IP address, and port number, for filtering, which are generally stored in a tabular format and are applied top to bottom.
      
      ![ACL Table](/assets/images/acl_table.jpg)  
      
      The disadvantage of this firewall is it filters only through checking the data header. It doesn't go through the data payload. 
  
   - They are the cheapest & quickest way to implement, as they are in routers. 

2. **Application / Proxy firewall:**
   - A proxy firewall acts as an intermediary between a trusted internal network and untrusted outside internet.
      
      ![proxy firewall image](/assets/images/proxy-firewall2.jpg)

   - A proxy firewall shields the internal network from intruders on the outside internet and prevents direct connections between the internal network and the internet.
   - An application firewall is generally slower, as it checks the data payload and header. But, it is more secure.
3. **Hybrid firewall:**
    - Hybrid firewalls enhance security by combining the packet filtering and application firewall in series.
      
      ![Hybrid firewall image](/assets/images/hybrid-firewall.jpg)

    - Suppose the two are connected in parallel instead of in series. In that case, the security gets reduced to the packet filtering method and is compromised, as it gets reduced to only packet filtering.
    - They give the best security by combining packet filtering and application firewall elements.
  
## Which firewall is suitable for us?
The following table helps choose the best firewall based on its implementation environment.
- 0 - Unacceptable
- 1 - Minimal Security
- 2 - Acceptable
- 3 - Effective
- 4 - Recommended

| Firewall Architecture | High-Risk Environment | Medium-Risk Environment | Low-Risk Environment  |
| --------------------- | --------------------- | ----------------------- | --------------------- |
|                       | Example: Hospital     | Example: University     | Example: Florist shop |
| Packet Filtering      | 0                     | 1                       | **4** &#10004;        |
| Application Gateways  | 3                     | **4**  &#10004;         | 2                     |
| Hybrid Gateways       | **4**  &#10004;       | 3                       | 2                     |


## Limitations of Firewall
Regarding network security, firewalls are considered the first line of defence. But, the question is whether firewalls are enough to safeguard us from cyber-attacks. Hence, it is essential to use other defence mechanisms to protect ourselves. Here are a few limitations of the firewall listed.
 - Firewalls cannot stop users from accessing malicious websites, making them vulnerable to internal threats or attacks.
 - Firewalls cannot protect against the transfer of virus-infected files or software, and one should have anti-virus software installed to save from such threats.
 - Firewalls cannot prevent misuse of passwords.
 - Firewalls cannot protect if security rules are not in the proper configuration.
 - Firewalls cannot protect against non-technical security risks, such as social engineering.
 - Firewalls cannot secure the system which is already infected.

---
## References
- [Firewall, Network Security by **Tech Terms Youtube channel**](https://www.youtube.com/watch?v=eO6QKDL3p1I&list=PLBbU9-SUUCwV7Dpk7GI8QDLu3w54TNAA6)  
- [Advantages & Disadvantages of Software & Hardware Firewall](https://www.anandsoft.com/networking/network-security-firewalls.html)
- [What is Data Packet / Network Packet](https://en.wikipedia.org/wiki/Network_packet)
