# Energy Management

## 1. Tiny Habits - BJ Fogg

### Q1. Your takeaways from the video (Minimum 5 points)
 
- Real change comes through hundreds of small decisions of small habits.
- One can make drastic changes with minor tweaks to the daily routine. It does not require something big.
- We generally underestimate the power of repetitively making smaller improvements over a longer period.
- Deciding the change needed into tiny habits, and adopting them one after another gradually.
- To develop a habit, have motivation, ability and a trigger.

## 2. Tiny Habits by BJ Fogg - Core Message

### Q2. Your takeaways from the video in as much detail as possible

To develop a habit, cultivate tiny habits, which leads to the habbit. Which can be done by following 3 process.

 - Shrink the behaviour/Take the first step: Shrink the behaviour into tinier habit which  requires little motivation and has bigger impact.

 - Identify an action prompt: There are three types of prompts - External, Internal and Action prompt. Use action prompt with regular behavior to trigger a new behavior rather than external or internal prompt.
  
 - Grow habits with some shine: Celebrate after every small victories, which increases the confidence and motivation to make progress.

### Q3. How can you use B = MAP to make making new habits easier?

B(Behaviour) = MAP(Motivation, Ability, Prompt)

This formula says, if you are trying to develop new habbit,
- 1. Reduce the quantity(Break into smaller chunk)/ Just do the first step.
- 2. Identify an action prompt, and attach it to the habit
- 3. Celebrate the small victories.
   
### Q4. Why it is important to "Shine" or Celebrate after each successful completion of habit?

Celebrating small victories and rewarding ourselves attracts us towards the habit of continuing for the long term and thus helps us become a better version of ourselves.

## 3. 1% Better Every Day Video
  
### Q5. Your takeaways from the video (Minimum 5 points)
 
- Habits develop and get internalise over time. 
- Create barriers for your bad habbits.
- Make good habbits easier to follow.
- Make a proper plan and have clarity for achieving any behaviour.
- Environment affect one's behaviour, so look for environment that is condusive.
- Plan for Optimizing the beginning, not the ending.
- Follow Two-minute rule: If it takes two minutes or less, do it instantly, don't procastinate it.
- Be consistent, and don't break the chain of small habbits.

## 4. Book Summary of Atomic Habits

### Q6. Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?
 
 - Outcomes are about what you get. Processes are about what you do. Identity is about what you believe.                

 - With outcome-based habits, the focus is on what you want to achieve. With identity-based habits, the focus is on who you wish to become.                

 - The ultimate form of intrinsic motivation is when a habit becomes part of your identity.

- Decide the type of person you want to be. Prove it to yourself with small wins.
  
- The most effective way to change your habits is to focus not on what you want to achieve, but on who you wish to become.

- Becoming the best version of yourself requires you to continuously edit your beliefs, and to upgrade and expand your identity.

### Q7. Write about the book's perspective on how to make a good habit easier?

A good habbit can be cultivated in four stages: 

  - **Cue**: Initiate the action, make the first move. Make the trigger obvious and easy to reach.
  - **Craving**: Have cravings which attract you towards it and make it look effortless.
  - **Response**: Make the habbit easier by creating fewer steps.
  - **Reward**: By Rewarding yourself for small victories and celebrating it.

### Q8. Write about the book's perspective on making a bad habit more difficult?

A habbit can be made more difficult by

 - Making the habbit harder.
 - Making it ugly to indulge in the behavior.
 - Increasing the friction of the behaviour.
 - Making the behavior immediately unsatisfying.

## 5. Reflection:

### Q9. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

I will read a blog everyday related to software engineering

 - I will subscribe to some standard channnels and blogs
 - I will work on the new things learnt and maintain a github profile and maintain it. 

### Q10. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

I will stop looking for easier methods, and rather choose standard methods of software engineering.

---
 ## References
- [Forget big change, start with a tiny habit: BJ Fogg at TEDxFremont](https://www.youtube.com/watch?v=AdKUJxjn-R8)
- [TINY HABITS by BJ Fogg | Core Message](https://www.youtube.com/watch?v=S_8e-6ZHKLs)
- [1% Better Every Day - James Clear at ConvertKit Craft + Commerce 2017](https://www.youtube.com/watch?v=mNeXuCYiE0U)
- [Sleep is your superpower | Matt Walker](https://www.youtube.com/watch?v=5MuIMqhT8DM)
- [Tiny Changes, Remarkable Results - Atomic Habits by James Clear](https://www.youtube.com/watch?v=YT7tQzmGRLA)
  