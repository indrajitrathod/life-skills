# Focus Management

## 1. What is Deep Work

### Q1. What is Deep Work?
- Focusing without distraction on a cognitively demanding task is called as Deep Work.
- Deep work increases quality of work.

## 4. Summary of Deep Work Book
   
### Q2. Paraphrase all the ideas in the above videos and this one in detail. 
 - Optimal duration for deep work is at least 1 hour. Not to switch between context for at least an hour during deep work. 
 - Deadlines/time-block schedules are kind of motivational signal, it helps one by avoiding procastinating things and avoid taking unneccessary breaks in middle.
 - Deadlines should be reasonale to humane capacity, too much pressure can reduce the productivity.
 - Breaks and distractions must be scheduled.
 - Deep work must be made as a habbit.
 - Get enough sleep.
  
### Q3. How can you implement the principles in your day to day life?
- Practice deep work with atleast one hour at a stretch, without switching context.
- Keep distractions aside.
- Don't look at smartphones, or social medias unneccessarily which wastes a lot of time, and reduce concentration.
- Making deep-work a habit, and practice it everyday.
- Having adequate sleep.

## 5. Dangers of Social Media

### Q4. Your key takeaways from the video
- Social media is not a fundamental technology. But it's a slot machin in the phone (it is a source of entertainment), which leverages some fundamental technologies.
 - Social medias offers shiny treats in exchange of minutes of user's attention and bytes of personal data.
 - Many of the social media companies hire attention engineers, who gather principles from gambling, and other such, and make these products as addictive as possible.
 - Focus on doing deep, concentrated work required to build real skills and to apply those skills to produce things that are valuable, rather than focusing on increasing social media followers, which can be done by anyone with a smartphone and copying/imitating.
 - Social media brings with it multiple, well-documented, and significant harms. Such as
   - Losing Ability to sustain concentration & Harm to professional success: as social medias are designed to be addictive, so much of the attention is lost to this. This permanently reduces the capacity to concentrate, and do more neccessary in a competitive economy.
   - Psychological loss: More the usage of social media, the more likely to feel loney or isolated, can make one feel inadequate and can increase rates of depression.
   - Using social media is like taking gambling machine everywehre and using it in waking hours, which can create a pervasive background hum of anxiety.
   - A report says, along with the rise of ubiquitous smartphone and social media on college campus, there is an explosion on anxiety-related disorders.
  -  Saying Social media usage is not harmless is not enough, one need to realise the potential harm social media brings with it's usage.
  -  It may feel discomfort after leaving social media, for around two weeks, but it's a true detox process.
  -  By treating the attention with respect, and preserving concentrating bby not getting distracted by social media, one can work sequentially, in order and with intensity, and intensity can be traded for time.
  -  Much people shouldn't be using social media as it's being used.
---
 ## Reference Videos
- [What is Deep Work? | Cal Newport and Lex Fridman](https://www.youtube.com/watch?v=b6xQpoVgN68)
- [Optimal duration for deep work is at least 1 hour | Cal Newport and Lex Fridman](https://www.youtube.com/watch?v=LA6mvxwecZ0)
- [Are deadlines good for productivity? | Cal Newport and Lex Fridman](https://www.youtube.com/watch?v=Jkl1vMNvvHU)
- [Success in a distracted world: DEEP WORK by Cal Newport](https://www.youtube.com/watch?v=gTaJhjQHcf8)
- [Quit social media | Dr. Cal Newport | TEDxTysons](https://www.youtube.com/watch?v=3E7hkPZ-HTk)
  