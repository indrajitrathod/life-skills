# Grit and Growth Mindset

## 1. Grit

### Q1. Paraphrase (summarize) the video in a few lines. Use your own words.

The video talks about the importance of grit and a growth mindset. It's about the significance of grit over IQ and other parameters. Grittier people are more likely to become successful than talented one. The only way to develop grit is through a growth mindset.

### Q2. What are your key takeaways from the video to take action on?

- For one to succeed in life, it's not just IQ but grit that matters most.
- Grit is **hunger** for a goal.
- Don't fall during failure. Instead, learn and improve.
- To perseverance during failure. 
- Having a growth mindset improves one's performance.

## 2. Introduction to Growth Mindset
   
### Q3. Paraphrase (summarize) the video in a few lines in your own words.

The video talks about the two kinds of mindsets
 -  **Growth Mindset** - How growth mindset can helps in progress and improve one's performance.
 -  **Fixed Mindset** - How a fixed mind leads to stagnation.

Many companies have implemented this growth mindset in their culture.


### Q4. What are your key takeaways from the video to take action on?
- Not to have a fixed mindset.
- Having Self-belief.
- To focus on the process of getting better at something over some time.
- Not getting discouraged during failure.

## 3. Understanding Internal Locus of Control

### Q5. What is the Internal Locus of Control? What is the key point in the video?
**Locus of Control** is the degree one's belief that, one has control over their life. And **Internal Locus of Control** is the belief that How much work one put into something that one has complete control over.

**Key Point in the video:** Internal Local of Control is the key for motivation.

To be motivated all the time, one can best achieve it by simply solving the issues in one's own life, taking some time, and appreciating that one action solved the problems.

## 4. How to build a Growth Mindset

### Q6. Paraphrase (summarize) the video in a few lines in your own words.


There are basically 2 kinds of mindset.
1. **Fixed Mindset** - When one thinks, things will always be the way they are, natural, and one can't grow.
2. **Growth Mindset** - When one believes that one can improve and grow through effort and actions.


Growth Mindset is popularised by **Carol Dweck** through his book 'Mindset: The New Psychology of Success'.

Gowth mindset can be developed by following methods,
 - **Belief:** Belief in your ability to figure things out.
 - **Questioning your Negative Assumptions:** Don't let your current knowledge & ability narrow down your future. What you are capable of doing today has nothing to do with what you can do tomorrow. Question assumptions about your ability if it's hindering your growth.
 - **Develop Life-curriculum:** One should develop a long-term curriculum for long-term growth. Develop an architect for the goal. For that, buy books, attend seminars, etc., and don't wait/depend on anyone or anything for your growth.
 - **Honour the struggle:** During the process, one may face many discouragement, failures, and pain. One should honour the struggle and have perseverance.

### Q7. What are your key takeaways from the video to take action on?
   - Self-belief and self-confidence are the base for growth.
   - Have a life curriculum, and work towards that.
   - Don't get frustrated during the process, and honour the struggle. The difficulty will build a stronger character and help subsequently, so keep going.
   - To keep persistance when getting knocked out. 


## 5. Mindset - A MountBlue Warrior Reference Manual

### Q8. What are one or more points that you want to take action on from the manual?

- I will stay with a problem till I complete it. I will not quit the problem.
- I will not write a word of code that I don’t understand.
- I will be focused on mastery till I can do things half asleep. If someone wakes me up at 3 AM in the night, I will calmly solve the problem like James Bond and go to sleep. 

---
 ## References
- [Grit: the power of passion and perseverance | Angela Lee Duckworth
](https://www.youtube.com/watch?v=H14bBuluwB8)
- [Growth Mindset Introduction: What it is, How it Works, and Why it Matters](https://www.youtube.com/watch?v=75GFzikmRY0)
- [How To Stay Motivated - The Locus Rule](https://www.youtube.com/watch?v=8ZhoeSaPF-k)
- [How to Develop a Growth Mindset](https://www.youtube.com/watch?v=9DVdclX6NzY)
- [A MountBlue Warrior Reference Manual](https://docs.google.com/document/d/1SPUqC-8WwfiDlsRGKWqoMtC14v6_2TEhq7LZs29bJWk/edit)
  