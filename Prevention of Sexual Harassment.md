## Prevention of Sexual Harassment at workplace
Sexual Harassment is any unwelcoming verbal,visual, or physical conduct of a sexual nature, that is severe or pervasive and affects working conditions or creates a hostile work environment.

**There are 3 forms of sexual harassment**
 1.  **Verbal**: It includes behaviour of folowing nature
        - comments about clothing
        - A person's body
        - Sexual or gender based jokes or remarks
        - Requesting sexual favours
        - repreatedly asking a person out
        - Sexual innuendos
        - Threats
        - Spreading runous about a peron's personal or sexual life
        - Using Foul and obscene language
 2.  **Visual**: These includes
        - Obscene posters
        - Drawings or Pictures
        - Screensavers
        - Cartoons
        - Emails or Texts of sexual nature
 3.  **Physical**: These includes
        - Sexual assault
        - Impeding or blocking movement
        - Sexual gesturing
        - leering or staring
        - Inappropriate touching such as 
            -  Kissing
            -  Hugging
            -  Padding
            -  Stroking
            -  Rubbing
  
### Categaries of Sexual Harassment
Each of these 3 forms of sexual harassment fall in these 2 categaries,

1. #### QUID PRO QUO - 
   The Latin term quid pro quo translates to "something for something."

    Therefore, quid pro quo harassment occurs in the workplace when a manager or other authority figure offers or merely hints that he or she will give the employee something (a raise or a promotion) in return for that employee's satisfaction of a sexual demand. This also occurs when a manager or other authority figure says he or she will not fire or reprimand an employee in exchange for some type of sexual favor. A job applicant also may be the subject of this kind of harassment if the hiring decision was based on the acceptance or rejection of sexual advances.
2. #### Hostile Work Environment - 
   A hostile work environment can be understood  in the context of an act of sexual harassment which occurs when an employee has been accustomed to unwelcome advances, sexual innuendos, or offensive gender-related language that is sufficiently severe or pervasive. Such instances can make it difficult for the victim to delegate their professional duties.

---
## Action to be taken 
 - In the case of me being a victim
 - Or witness any harassment

1.    **Talk to the harasser:** I will inform him about his offensive behaviour and be as specific as possible. And tell the person to stop doing it. If that does not resolve the problem, I will collect every detail, witness and keep a record.
2. **Complain to my supervisor:** I would inform regarding offensive behaviour to my superiors if the harasser doesn't stop even after talking. Follow up by putting my complaint in writing and keeping a copy of it for later use.
3. **Follow the company's internal complaint process:** I would follow the procedures and complaint with ICC(Internal Complain Committee) and keep a record of it.
4. **Keep a journal:** Keep a journal about all incidents of suspected sexual harassment and my attempts to resolve the problem. I will note the date, time, and witnesses to each incident.
5. **Keep copies of anything offensive:** I will maintain copies or pictures of any offensive posters, notes, texts, messages, or 
6. **File a complaint**: In extreme cases, I will file a police complaint or contact an attorney.

---
 ## References
- [Sexual Harassment Overview](https://www.youtube.com/watch?v=Ue3BTGW3uRQ)  
- [What Does Bullying and Harassment Mean for You and Your Workplace?](https://www.youtube.com/watch?v=u7e2c6v1oDs)
- [Sexual Harassment - ifferent scenarios and what action to take](https://www.youtube.com/watch?v=o3FhoCz-FbA)

